(defproject hps-sana "1.0.0-SNAPSHOT"
  :description "A Heuristic Problem Solver Application"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/tools.logging "0.4.0"]
                 [org.slf4j/slf4j-log4j12 "1.7.25"]
                 [log4j/log4j "1.2.17" :exclusions [javax.mail/mail
                                                    javax.jms/jms
                                                    com.sun.jmdk/jmxtools
                                                    com.sun.jmx/jmxri]]
                 [org.clojure/tools.cli "0.3.5"]
                 [cfft/cfft "0.1.0"]
                 [amalloy/ring-buffer "1.2.1"]
                 [org.clojure/core.async "0.4.474"]
                 [incanter/incanter-core "1.9.1"]
                 [com.novemberain/monger "3.1.0"]
                 [nz.ac.waikato.cms.weka/weka-stable "3.8.1"]
                 [http-kit "2.2.0"]
                 [heuristic-problem-solver "1.0.0-SNAPSHOT"]]
  :java-source-paths ["src/java"]
  :javac-options ["-Xlint:deprecation,unchecked"]
  :prep-tasks [["compile" "hps.types"]
               "javac" "compile"]
  :aot [sana.core]
  :main sana.core)
