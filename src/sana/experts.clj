;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns sana.experts (:require [hps.types :refer :all] [hps.decision-modules :refer :all] [hps.heuristics :refer :all] [hps.make-decision :as md] [hps.utilities.math :as mut] [hps.utilities.misc :as utmisc] [sana.state :as state] [sana.data :as data] [sana.server :as server] [sana.classify :as cf]))


(defn wifiprofilereval "Looks up the current wifiprofile and applies the profile certainty as evaluation on the\n  proposed position value." [alts] (let [profile (cf/classify-wifi-profile server/db)] (if (nil? profile) (zipmap alts (map (fn* [p1__1221#] (if (= (:value p1__1221#) :UnknownLocation) 1.0 0.5)) alts)) (zipmap alts (map (fn* [p1__1066#] (or (get profile (keyword (:value p1__1066#))) 0.0)) alts)))))

(def-heuristic :WorkPCScreen
  :producer (def-expert-fun [] :WorkPCScreen)
  :evaluator (def-expert-fun [alts] (zipmap alts (repeat (count alts) 1.0))))


(def-heuristic :KnownPlaces
  :producer (def-expert-fun [] (let [result (map (fn* [p1__1065#] (:alias p1__1065#)) (server/knownplaces))] result))
  :evaluator (def-expert-fun [alts] (zipmap alts (repeat (count alts) 1.0))))


(def-heuristic :GeneralUsageMatrix :producer (def-expert-fun [] :GeneralUsageMatrix) :evaluator (def-expert-fun [alts] (zipmap alts (map (fn* [alt] (get data/general-usage (:value alt))) alts))))



(def-heuristic :UnknownLocation
  :producer (def-expert-fun [] :UnknownLocation)
  :evaluator (def-expert-fun [alts] (zipmap alts (repeat (count alts) 1.0))))


(def-heuristic :Mobile :producer (def-expert-fun [] :Mobile) :evaluator (def-expert-fun [alts] (zipmap alts (repeat (count alts) 1.0))))

(def-heuristic :GeneralResponse :producer (def-expert-fun [] :GeneralResponse) :evaluator (def-expert-fun [alts] (zipmap alts (map (fn* [p1__1077#] (server/pullGeneralResponse (:value p1__1077#))) alts))))



(def-heuristic :Smartphone
  :producer (def-expert-fun [] :Smartphone)
  :evaluator (def-expert-fun [alts] (zipmap alts (repeat (count alts) 1.0))))




(def-heuristic :PositionHabits
  :producer (def-expert-fun [] :PositionHabits)
  :evaluator (def-expert-fun [alts] (zipmap alts (repeat (count alts) 1.0))))




(def-heuristic :HomeTV
  :producer (def-expert-fun [] :HomeTV)
  :evaluator (def-expert-fun [alts] (zipmap alts (repeat (count alts) 1.0))))




(def-heuristic :HomePCScreen
  :producer (def-expert-fun [] :HomePCScreen)
  :evaluator (def-expert-fun [alts] (zipmap alts (repeat (count alts) 1.0))))




(def-heuristic :PositionalChannelApproval
  :producer (def-expert-fun [] :PositionalChannelApproval)
  :evaluator (def-expert-fun [alts] (zipmap alts (map (fn* [alt] (server/pullPositionChannelApproval (:value alt) @state/position)) alts))))


(def-heuristic :PositionalUsageMatrix
  :producer (def-expert-fun [] :PositionalUsageMatrix)
  :evaluator (def-expert-fun [alts] (zipmap alts (map (fn* [alt] (server/pullPositionDeviceResponse (:value alt))) alts))))




(def-heuristic :Tactile
  :producer (def-expert-fun [] :Tactile)
  :evaluator (def-expert-fun [alts] (zipmap alts (repeat (count alts) 1.0))))




(def-heuristic :Random
  :producer (def-expert-fun [] :Random)
  :evaluator (def-expert-fun [alts] (zipmap alts (repeatedly (count alts) (fn* [] (+ 0.5 (rand 0.5)))))))




(def-heuristic :WifiProfiler
  :producer (def-expert-fun [] :WifiProfiler)
  :evaluator (def-expert-fun [alts] (wifiprofilereval alts)))




(def-heuristic :Visual
  :producer (def-expert-fun [] :Visual)
  :evaluator (def-expert-fun [alts] (zipmap alts (repeat (count alts) 1.0))))


(def-heuristic :Evaluator0 :producer (def-expert-fun [] :Evaluator0) :evaluator (def-expert-fun [alts] (zipmap alts (repeat (count alts) 1.0))))

(def-heuristic :GaneralUsageMatrix :producer (def-expert-fun [] :GaneralUsageMatrix) :evaluator (def-expert-fun [alts] (zipmap alts (repeat (count alts) 1.0))))

(def-heuristic :Producer0 :producer (def-expert-fun [] :Producer0) :evaluator (def-expert-fun [alts] (zipmap alts (repeat (count alts) 1.0))))



(def-heuristic :DevicePriorityApproval
  :producer (def-expert-fun [] :DevicePriorityApproval)
  :evaluator (def-expert-fun [alts] (zipmap alts (map (fn* [alt] (server/pullPositionDeviceResponse (:value alt))) alts))))




(def-heuristic :Audio
  :producer (def-expert-fun [] :Audio)
  :evaluator (def-expert-fun [alts] (zipmap alts (repeat (count alts) 1.0))))




(def-heuristic :TimedChannelApproval
  :producer (def-expert-fun [] :TimedChannelApproval)
  :evaluator (def-expert-fun [alts] (zipmap alts (map (fn* [alt] (server/pullTimedChannelApproval (:value alt) @state/hour)) alts))))




(def-heuristic :Smartwatch
  :producer (def-expert-fun [] :Smartwatch)
  :evaluator (def-expert-fun [alts] (zipmap alts (repeat (count alts) 1.0))))




(def-heuristic :ChannelPriorityApproval
  :producer (def-expert-fun [] :ChannelPriorityApproval)
  :evaluator (def-expert-fun [alts] (zipmap alts (map (fn* [p1__1077#] (server/pullChannelPriorityApproval (:value p1__1077#) (clojure.core/deref state/priority))) alts))))


(declare-dm :DeviceChooser
            :available-experts {:producers [:WorkPCScreen :HomePCScreen :Smartphone :HomeTV :Smartwatch]
                                :evaluators [:Random :PositionalUsageMatrix :DevicePriorityApproval]}
            :configure-fun (fn [_]
                             (map->DecisionSetup
                              {:experts {:producers [:WorkPCScreen :HomePCScreen :Smartphone :HomeTV :Smartwatch]
                                         :evaluators [:Random :PositionalUsageMatrix :DevicePriorityApproval]}}))
            :onacceptance-fun (fn [dec] nil)
            :decision-parameters {:evaluator-aggregation-fun [:weighted-sum :raw]
                                  :run-frequency-fun (run-frequency-fun-by-time 1000)
                                  :clocked-interval 1})


(declare-dm :PositionEstimator
            :available-experts {:producers [:UnknownLocation :KnownPlaces]
                                :evaluators [:WifiProfiler :PositionHabits]}
            :configure-fun (fn [_]
                             (map->DecisionSetup
                              {:experts {:producers [:UnknownLocation :KnownPlaces]
                                         :evaluators [:WifiProfiler :PositionHabits]}}))
            :onacceptance-fun (fn [dec] (reset! state/position (:value dec)))
            :decision-parameters {:evaluator-aggregation-fun [:weighted-sum :raw]
                                  :run-frequency-fun (run-frequency-fun-by-time 1000)
                                  :clocked-interval 1})


(declare-dm :ChannelPicker
            :available-experts {:producers [:Audio :Visual :Tactile]
                                :evaluators [:Random :TimedChannelApproval :PositionalChannelApproval :ChannelPriorityApproval]}
            :configure-fun (fn [_]
                             (map->DecisionSetup
                              {:experts {:producers [:Audio :Visual :Tactile]
                                         :evaluators [:Random :TimedChannelApproval :PositionalChannelApproval :ChannelPriorityApproval]}}))
            :onacceptance-fun (fn [dec] nil)
            :decision-parameters {:evaluator-aggregation-fun [:weighted-sum :raw]
                                  :run-frequency-fun (run-frequency-fun-by-time 1000)
                                  :clocked-interval 1})
