(ns sana.decoder
  (:import [java.nio ByteBuffer]))


(defn bbytes->string [bytebuffer start length]
  (String. (byte-array (subvec (vec (.array bytebuffer)) start (+ start length)))))
(defn get-accel-window [buffer start len]
  (let [indices (map #(+ start (* 4 %)) (range len))]
    (partition 3 (map #(.getFloat buffer %) indices))))

(defn bbytes->farray [buffer start len]
  (let [indices (map #(+ start (* 4 %)) (range len))]
    (map #(.getFloat buffer %) indices)))


(defn decodeinstruction [inst bbuffer offset]
  (cond
    (= inst :int) [(.getInt bbuffer offset) 4]
    (= inst :long) [(.getLong bbuffer offset) 8]
    (= inst :string) (let [strlen (.getInt bbuffer offset)]
                       [(bbytes->string bbuffer (+ offset 4) strlen) (+ 4 strlen)])
    (= inst :float-array) (let [alen (.getInt bbuffer offset)]
                            [(bbytes->farray bbuffer (+ offset 4) alen) (* 4 (inc alen))])))

(defn decodepacket [bytebuffer instructions]
  (loop [remain (rest instructions)
         instruction (first instructions)
         offset 0
         result []]
    (let [[data datasize] (decodeinstruction instruction bytebuffer offset)]
      ;; (println "decoded " data " of size " datasize)
      (if (empty? remain) (conj result data)
          (recur (rest remain) (first remain) (+ offset datasize) (conj result data))))))
