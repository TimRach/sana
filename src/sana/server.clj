(ns sana.server
  (:refer-clojure :exclude [sort find])
  (:require [sana.classify :as cf]
            [sana.decoder :as dc]
            [sana.state :as state]
            [sana.data :as data]
            [hps.decision-modules :as dms]
            [monger.core :as mg]
            [monger.operators :refer :all]
            [monger.collection :as mc]
            [monger.query :refer :all]
            [org.httpkit.server :refer :all]
            [hps.decision-modules :as dms])
  (:import (java.net InetAddress DatagramPacket DatagramSocket)
           (java.util Locale)
           (java.time LocalDateTime Instant ZoneId)
           [com.mongodb MongoOptions ServerAddress]
           (java.time.format TextStyle FormatStyle DateTimeFormatter)
           (java.nio ByteBuffer)))

(def db (mg/get-db (mg/connect) "stattrackdata"))


(defn knownplaces []
  (mc/find-maps db "wifiprofiles" {}))


(defn readAccelerometerData [bytebuffer targetbuffer]
  (let [[user device data] (dc/decodepacket bytebuffer [:int :int :float-array])]
    (swap! targetbuffer conj data)))


(defn insertDocument [data]
  (let [entry (zipmap [:user :day :time :phone :watch :hrate :steps :wlan :bluetooth] data)]
    (swap! state/entrybuffer conj entry)
    (mc/insert db "statedata" entry)))


(defn readStateData [bbuffer]
  (let [[user device time heartrate
         stepdata bluetooth wifi] (dc/decodepacket bbuffer [:int :int :long :int :int :string :string])
        timestamp (LocalDateTime/ofInstant (Instant/ofEpochMilli time) (ZoneId/systemDefault))
        timeformat (.format timestamp DateTimeFormatter/ISO_LOCAL_DATE_TIME)
        day (.getDisplayName (.getDayOfWeek timestamp) TextStyle/SHORT Locale/GERMAN)
        pclass (cf/classify-phone (cf/make-features @state/phonebuffer))
        wclass (cf/classify-watch (cf/make-features @state/watchbuffer))]
    (swap! state/phoneclassbuffer conj pclass)
    (swap! state/watchclassbuffer conj wclass)
    (swap! state/hratebuffer conj heartrate)
    (swap! state/stepbuffer conj stepdata)
    (reset! state/wlanconnection wifi)
    (reset! state/wlanconnection bluetooth)
    (insertDocument [user day timeformat (last @state/phoneclassbuffer)
                     (last @state/watchclassbuffer) (last @state/hratebuffer)(last @state/stepbuffer)
                     (clojure.string/split wifi #",") (clojure.string/split bluetooth #",")])
    (cf/classify-wifi-profile db)
    (println user "," day "," timeformat "," (last @state/phoneclassbuffer) "," (last @state/watchclassbuffer) ","
             (last @state/hratebuffer) "," (last @state/stepbuffer) "," wifi ",\"" bluetooth "\"")))



(defn synchronize-packet [bytebuffer]
  (let [[user device timestamp phone watch
         step heartrate bluetooth wifi] (dc/decodepacket bytebuffer [:int :int :long :float-array
                                                                     :float-array :int :int :string :string])
        time (LocalDateTime/ofInstant (Instant/ofEpochMilli timestamp) (ZoneId/systemDefault))
        timef (.format time  DateTimeFormatter/ISO_LOCAL_DATE_TIME)
        day (.getDisplayName (.getDayOfWeek time) TextStyle/SHORT Locale/GERMAN)
        pclass (cf/classify-phone (cf/make-features (partition 3 phone)))
        wclass (cf/classify-watch (cf/make-features (partition 3 watch)))
        ]
    (insertDocument [user day timef pclass wclass heartrate step
                     (clojure.string/split wifi #",") (clojure.string/split bluetooth #",")])
    (println (str user "," timef "," day "," pclass "," wclass "," heartrate "," bluetooth "," (str (cf/classify-wifi-profile db))))))


(defn registerclient [user deviceid]
  (let [userentry (first (with-collection db "users" (find {:user user})))]
    (if (nil? userentry)
      (do (println "new user")
          (mc/insert db "users" {:user user :devices [deviceid]}))
      (do (println "known user")
          (if (some #{deviceid} (:devices userentry))
            (println "device already known")
            (do (println "device unknown")
                (mc/update db "users" {:user user} {$push {:devices deviceid}})))))))


(defn respond [packet channel]
  (println "Respond " packet)
  (send! channel packet))

;;current websocket channel
(def thechannel (atom nil))

(defn safeDiv [a b]
  (if (zero? b)
    0
    (float (/ a b))))

(defn pullPositionDeviceResponse [device]
  (safeDiv (mc/count db "notifications" {:accepted true :targetdevice (name device) :location @state/position})
           (mc/count db "notifications" {:targetdevice (name device) :location @state/position})))


(defn pullDevicePriorityApproval [device prio]
  (safeDiv (mc/count db "notifications" {:accepted true :priority prio :targetdevice (name device)})
           (mc/count db "notifications" {:priority prio :targetdevice (name device)})))


(defn pullChannelPriorityApproval [channel prio]
  (safeDiv (mc/count db "notifications" {:accepted true :priority prio :targetchannel (name channel)})
           (mc/count db "notifications" {:priority prio :targetchannel (name channel)})))


(defn pullGeneralResponse [channel]
  (safeDiv (mc/count db "notifications" {:accepted true :targetchannel (name channel)})
           (mc/count db "notifications" {:targetchannel (name channel)})))

(defn timestamphour [ts]
  (.getHour (LocalDateTime/ofInstant
             (Instant/ofEpochMilli ts)
             (ZoneId/systemDefault))))

(defn pullTimedChannelApproval [channel hour]
  (safeDiv
   (count (filter #(= hour (timestamphour (:creationdate %)))
                  (mc/find-maps db "notifications" {:accepted true :targetchannel (name channel)})))
   (count (filter #(= hour (timestamphour (:creationdate %)))
                  (mc/find-maps db "notifications" {:targetchannel (name channel)})))))

(defn pullPositionChannelApproval [channel pos]
  (safeDiv (mc/count db "notifications" {:accepted true :targetchannel (name channel) :location pos})
           (mc/count db "notifications" {:targetchannel (name channel) :location pos})))



(defn send-debug-notification [arg]
  (send! @thechannel (str arg "-" 0 "-" 42)))


(defn sendnotification [n]
  (println "send notification " (:targetdevice n) (:targetchannel n) (:priority n) (:location n))
  (send! @thechannel (str (:message n) "-"
                          (:targetdevice n) "-"
                          (:targetchannel n) "-"
                          (:notificationID n))))


(defn createnotification [userentry]
  (let [prio @state/priority]
    (reset! state/priority (get [:High :Medium :Low] (rand-int 3)))
    {:user (:user userentry) :accepted false :creationdate (System/currentTimeMillis)
     :accepteddate 0 :delivered false :priority prio
     ;; :targetdevice (get (:devices userentry) (rand-int (count (:devices userentry))))
     ;; :targetchannel (get data/channels (rand-int 3))
     :targetdevice (or (:value @(:executed-decision
                                 (dms/get-dm-from-name :DeviceChooser 'sana.experts))) :Smartphone)
     :targetchannel (or (:value @(:executed-decision
                                  (dms/get-dm-from-name :ChannelPicker 'sana.experts))) :Tactile)
     :location @state/position
     :message (str "Priority " prio " message") :notificationID (rand-int (Integer/MAX_VALUE))}))

(defn pendingnotification
  "Checks if the database contains a pending notification and returns it if one exists and it has not expired.
  If there is no pending notification check if it is time for a new one and create and return it. Otherwise return nil."
  [user]
  (if-let [userentry (mc/find-one-as-map db "users" {:user user})]
    (let [lastnot (first (with-collection db "notifications" (find {:user user}) (sort {:creationdate -1})))]
      (if (or (nil? lastnot)
              (> (- (System/currentTimeMillis) (:creationdate lastnot)) @state/notificationage))
        (do (println "notification found was too old -> create a new one")
            (state/nextnotificationtime)
            (mc/insert-and-return db "notifications" (createnotification userentry)))
        (do (println "found notification was young enough - notification was nil:" (nil? lastnot))
            (if (:delivered lastnot)
              (println "Notification already was delivered -do not send again")
              lastnot))))
    (println "no user for pending notification lookup. this shouldn't happen.")))

(defn onnotificationrequested
  "a user device requested an update on pending notifications"
  [channel user deviceid]
  (println "user" user "requested notification for device " deviceid)
  ;; (registerclient user deviceid)
  (when-let [notification (pendingnotification user)]
    (sendnotification notification)))

(defn onnotificationdismissed
  "a user saw and dismissed a delivered notification"
  [channel user device notificationid]
  (mc/update db "notifications" {:user user :notificationID notificationid} {$set {:accepted true}})
  (mc/update db "notifications" {:user user :notificationID notificationid} {$set {:accepteddate (System/currentTimeMillis)}})
  (println "dismiss notification " notificationid " for user " user " on device " device))

(defn onnotificationdelivered
  "the target device responded to confirm that the notification was delivered"
  [user device notificationid]
  (println "Notification was delivered")
  (mc/update db "notifications" {:user user :notificationID notificationid} {$set {:delivered true}}))

(defn process-input-data [data channel]
  (let [bb (ByteBuffer/wrap data)
        user (.getInt bb 0)
        code (.getInt bb 4)]
    (cond
      (= code 0) (readAccelerometerData bb state/phonebuffer)
      (= code 1) (readAccelerometerData bb state/watchbuffer)
      (= code 2) (readStateData bb)
      (= code 3) (synchronize-packet bb)
      ;; (= code 1336) (do (registerclient user data) (send-notification))
      (= code 1337) (onnotificationrequested channel user (.getInt bb 8))
      (= code 3141) (onnotificationdelivered user (.getInt bb 8) (.getInt bb 12))
      (= code 4242) (onnotificationdismissed channel user (.getInt bb 8) (.getInt bb 12))
      :default (println "unknown command" code))))

(defn handler [request]
  (with-channel request channel
    (on-close channel (fn [status] (println "channel closed: " status)))
    (on-receive channel (fn [data] ;; echo it back
                          (reset! thechannel channel)
                          (process-input-data data channel)
                          (send! channel data)))))

(defn bootstrap []
  (reset! cf/watchmodel (.readObject (java.io.ObjectInputStream. (java.io.FileInputStream. "WATCH.model"))))
  (reset! cf/phonemodel (.readObject (java.io.ObjectInputStream. (java.io.FileInputStream. "PHONE.model")))))

(defn startserver []
  (bootstrap)
  (run-server handler {:port 9090}))
