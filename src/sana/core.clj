;; Copyright 2017

;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at

;; http://www.apache.org/licenses/LICENSE-2.0

;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns sana.core
  (:gen-class
   :extends javafx.application.Application)
  (:require [clojure.java.io :as io]
            [clojure.tools.cli :as parse-tool]
            [sana.state :as state]
            [sana.server :as server]
            [sana.GUI.core]
            [clojure.pprint :as pprint]
            [clojure.test :as test]
            [hps.GUI.main]
            [hps.GUI.structure :as structure]
            [hps.utilities.math :as mut]
            [clojure.tools.logging :as log])
  (:import javafx.scene.image.Image
           javafx.stage.Stage))

(defn error-msg [errors]
  (str "The following errors occurred while parsing your command:\n\n"
       (str \newline errors)))

(defn exit [status msg]
  (println msg)
  (System/exit status))

(defn -start [app ^Stage stage]
  (.add (.getIcons stage) (Image. (io/input-stream (io/resource "images/startimage.png"))))
  (doto stage
    (.setTitle "sana")
    (.setScene (structure/build-basic-hps-scene))
    (.show)))

(def cleanup-fun-at-stop (atom nil))

(defn -stop [_]
  (when (test/function? @cleanup-fun-at-stop)
    (@cleanup-fun-at-stop))
  (System/exit 0))


(defn -main [& args]
  (future (javafx.application.Application/launch sana.core (into-array String [])))
  (server/startserver))


(.addShutdownHook (Runtime/getRuntime)
                  (Thread. (fn [] (log/info "============= EXIT HPS =============="))))
