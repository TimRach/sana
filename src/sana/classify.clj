(ns sana.classify
  (:require [cfft.core] [cfft.matrix] [cfft.complex]
            [clojure.math.combinatorics :as comb]
            [incanter.stats :as stats]
            [sana.state :as state]
            [monger.collection :as mc]
            [sana.profile :as prof])
  (:import [weka.core Instances DenseInstance Attribute FastVector]))


;; =========== Action classification ===========
(def phonemodel (atom nil))
(def watchmodel (atom nil))
(def watchclassvalues ["ARM_90_PALM_DOWN" "ARM_90_PALM_UP" "ARM_90_HOLDING" "ARM_MOVING" "ARM_DOWN" "ARM_UP"])
(def phoneclassvalues ["STILL" "LEG_SIT""LEG_MOVING" "LEG_STAND" ])
(def numericattributes ["MEAN_X" "MEAN_Y"	"MEAN_Z"	"MEAN_A"	"VAR_X"	"VAR_Y"	"VAR_Z"	"VAR_A"	"CORRXY"	"CORRXZ"	"CORRXA"	"CORRYZ"	"CORRYA"	"CORRZA"	"ENERGY_X"	"ENERGY_Y"	"ENERGY_Z"	"ENERGY_A"])

(defn vector-length
  [v]
  (let [res (Math/sqrt (reduce + (map #(Math/pow % 2) v)))]
    res))


(defn average [numbers]
  (when (seq numbers) (/ (apply + numbers) (count numbers))))


(defn preprocessData
  "Adds the acceleration to each [x y z] row"
  [data]
  (mapv #(conj % (vector-length %)) data))


(defn make-features [window]
  (when-not (empty? window)
    (let [transposed (apply map list (preprocessData window)) ;Transforms [[x,y],[x,y]..] to [[x,x,..],[y,y,..]]
          means (map average transposed)
          vars (map stats/variance transposed)
          corrs (map #(stats/correlation (first %) (second %)) (comb/combinations transposed 2))
          fftnrg (map #(/ (vector-length (cfft.matrix/matrix-apply cfft.complex/real (cfft.core/fft %)))
                          (count %)) transposed)]
      (concat means vars corrs fftnrg))))

(defn makeInstance [featvec dataset]
  (let [inst (DenseInstance. (inc (count featvec)))
        attfeat (map list (range (inc (count featvec))) featvec)]
    (doseq [pair attfeat]
      (.setValue inst (first pair) (second pair)))
    (.setDataset inst dataset)
    inst))

(defn makeinitialset [classvalues]
  (let [numattrs (mapv #(Attribute. %) (seq numericattributes))
        classattr (Attribute. "CLASS" (seq classvalues))
        attributes (conj numattrs classattr)
        attinfo (FastVector. (count attributes))]
    (doseq [attr attributes]
      (.addElement attinfo attr))
    (let [instances (Instances. "Initial" attinfo 0)]
      (.setClassIndex instances (dec (.numAttributes instances)))
      instances)))

(def initialphoneset (makeinitialset phoneclassvalues))
(def initialwatchset (makeinitialset watchclassvalues))


(defn classify [features model initialset classvalues]
  (if (nil? features)
    "UNKNOWN"
    (let [instance (makeInstance features initialset)
          eval (weka.classifiers.Evaluation. initialset)]
      (.useNoPriors eval)
      (keyword (get classvalues (int (.evaluateModelOnce eval model instance)))))))

(defn classify-phone [features]
  (classify features @phonemodel initialphoneset phoneclassvalues))

(defn classify-watch [features]
  (classify features @watchmodel initialwatchset watchclassvalues))

(defn classify-wifi-profile
  ([db] (classify-wifi-profile db
                               (prof/make-wifi-profile
                                (last (prof/make-chunks @state/entrybuffer (count @state/entrybuffer))))))
  ([db wifiprofile]
   (let [raw  (vec (mc/find-maps db "wifiprofiles"))
         profiles (mapv :profile raw)
         all (map #(zipmap (map name (keys %)) (vals %)) profiles)
         similarities (map #(prof/profile-jaccard % wifiprofile) all)
         knownplaces (filter #(> % 0.4) similarities)
         placeindexes (map #(.indexOf similarities %) knownplaces)]
     (if (empty? knownplaces)
       (when-not (empty? wifiprofile)
         (println wifiprofile)
         (println "found new location --> update db")
         (mc/insert-and-return db "wifiprofiles" {:alias (str "profile" (count all))
                                                  :profile wifiprofile}))
       (do
         ;; (println "i know this - it is one of" (zipmap
         ;;                                        (map #(keyword (:alias (get raw %))) placeindexes)
         ;;                                        knownplaces))
         (zipmap (map #(keyword(:alias (get raw %))) placeindexes) knownplaces))))))
