;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns sana.state
  (:require [hps.decision-modules :refer :all]
            [hps.make-decision :refer :all]
            [hps.heuristics :refer :all]
            [amalloy.ring-buffer :as rb]))

(def hour (atom 9))
(def isRunning (atom false))
(def position (atom :UnknownLocation))
(def priority (atom :Low))

(def watchbuffer (atom []))
(def notificationage (atom 20000))
(def phonebuffer (atom []))
(def phoneclassbuffer (atom (rb/ring-buffer 10)))
(def watchclassbuffer (atom (rb/ring-buffer 10)))
(def stepbuffer (atom (rb/ring-buffer 10)))
(def hratebuffer (atom (rb/ring-buffer 10)))
(def wlanconnection (atom ""))
(def btconnection (atom ""))
(def entrybuffer (atom (rb/ring-buffer 3)))


(def statevars {:hour hour
                :isRunning isRunning
                :position position
                :priority priority
                :phoneclass phoneclassbuffer
                :watchclass watchclassbuffer})

(defn nextnotificationtime
  "resets the timespan a user can dismiss the current notification until a new one will be generated to
  a random timespan between 60 seconds and 30 minutes"
  []
  ;; (reset! notificationage (+ 60000 (rand-int 1800000)))
  (reset! notificationage (+ 30000 (rand-int 30000)))
  (println "Next notification in " (float (/ @notificationage 1000)) " seconds"))

;;Following functions are set as watches on the GUI controls. See GUI/controls.clj.
(defn on-slider-changed [slidervalue]
  (println "GUI Slider value changed"))

(defn on-step-fwd []
  (println "Step forward button pushed"))

(defn on-step-back []
  (println "Step back button pushed"))

(defn on-run []
  (println "Run button pushe")
  (doseq [dm (list-decision-modules 'sana.experts)]
    (when-not @isRunning
      (.start (Thread. (fn [] (start-dm dm 'sana.experts)
                         (println "Start decision module " dm))))))
  (reset! isRunning true))


(defn on-pause []
  (println "Pause button pushe")
  (doseq [dm (list-decision-modules 'sana.experts)]
    (when @isRunning
      (stop-dm dm 'sana.experts)
      (println "Stopped decision module " dm)))
  (reset! isRunning true))
