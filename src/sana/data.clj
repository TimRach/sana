(ns sana.data
  (:require
   [monger.collection :as mc]))


(def channels ["Visual" "Tactile" "Audio"])
(def general-usage {:Smartwatch 0.75 :Smartphone 0.6 :WorkPCScreen 0.3 :HomeTV 0.1 :HomePCScreen 0.1})
(def positional-usage {:Home {:Smartwatch 0.75 :Smartphone 0.6 :WorkPCScreen 0.0 :HomeTV 0.1 :HomePCScreen 0.1}
                       :Office {:Smartwatch 0.75 :Smartphone 0.1 :WorkPCScreen 0.6 :HomeTV 0.0 :HomePCScreen 0.0}
                       :Move {:Smartwatch 0.75 :Smartphone 0.6 :WorkPCScreen 0.0 :HomeTV 0.0 :HomePCScreen 0.0}})
(def priority-approval {:Low {:Smartwatch 0.1 :Smartphone 0.2 :WorkPCScreen 0.3 :HomeTV 0.8 :HomePCScreen 1.0}
                        :Medium {:Smartwatch 0.4 :Smartphone 0.5 :WorkPCScreen 0.5 :HomeTV 0.4 :HomePCScreen 0.3}
                        :High  {:Smartwatch 0.9 :Smartphone 0.7 :WorkPCScreen 0.8 :HomeTV 0.1 :HomePCScreen 0.1}})
(def channel-priority-approval
  {:Low {:Visual 0.8 :Tactile 0.2 :Audio 0.0}
   :Medium {:Visual 0.4 :Tactile 0.6 :Audio 0.1}
   :High  {:Visual 0.1 :Tactile 0.5 :Audio 0.7}})
