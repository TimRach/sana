(ns sana.profile
  (:refer-clojure :exclude [sort find])
  (:require [monger.core :as mg]
            [clojure.pprint :as pp]
            [monger.collection :as mc]
            [monger.core :as mg]
            [monger.query :refer :all]
            [clojure.set :as cs]
            [clojure.math.combinatorics :as comb]))

(defn zdiv [a b]
  (if (zero? b) 0 (/ a b)))


(defn minusDates [a b]
  (let [from (if (.isAfter a b) b a)
        to (if (= from a) b a)
        tempDate (java.time.LocalDateTime/from from)
        years (.until tempDate to java.time.temporal.ChronoUnit/YEARS)
        tempDate2 (.plusYears tempDate years)
        months (.until tempDate2 to java.time.temporal.ChronoUnit/MONTHS)
        tempDate3 (.plusMonths tempDate2 months)
        days (.until tempDate3 to java.time.temporal.ChronoUnit/DAYS)
        tempDate4 (.plusDays tempDate3 days)
        hours (.until tempDate4 to java.time.temporal.ChronoUnit/HOURS)
        tempDate5 (.plusHours tempDate4 hours)
        minutes (.until tempDate5 to java.time.temporal.ChronoUnit/MINUTES)
        tempDate6 (.plusMinutes tempDate5 minutes)
        seconds (.until tempDate6 to java.time.temporal.ChronoUnit/SECONDS)]
    (+ seconds (* 60 minutes) (* 60 60 hours) (* 60 60 24 days) (* 60 60 24 30 months) (* 60 60 24 30 12 years))))


(defn make-chunk [data size]
  (loop [remain (rest data)
         chunk [(first data)]]
    (if (or (empty? remain)
            (= (count chunk) size)
            (> (minusDates (java.time.LocalDateTime/parse (:time (first remain)))
                           (java.time.LocalDateTime/parse (:time (last chunk))))
               60))
      chunk
      (recur (rest remain) (conj chunk (first remain))))))

(defn make-chunks [data size]
  (loop [remain data
         chunks []]
    (if (empty? remain)
      chunks
      (let [chunk (make-chunk remain size)]
        (recur (drop (count chunk) remain) (conj chunks chunk))))))


(defn has-wlan [x]
  (not (and (<= (count (:wlan x)) 1)
            (= (first (:wlan x)) ""))))

(defn make-wifi-profile [chunk]
  (let [vslices (filter has-wlan chunk)
        freqs (frequencies (filter #(not= "" %) (reduce concat (map :wlan vslices))))]
    ;; (pp/pprint freqs)
    (zipmap (map first freqs) (map #(float (zdiv (last %) (count vslices))) freqs))))

(defn profile-strength [profile]
  (float (zdiv (apply + (map last profile)) (count profile))))

(defn profile-jaccard [p1 p2]
  (let [insec (clojure.set/intersection (set (keys p1)) (set (keys p2)))
        unio (clojure.set/union (set (keys p1)) (set (keys p2)))]
    (float (zdiv (count insec) (count unio)))))

(defn merge-profiles [p1 p2]
  (let [s1 (set (keys p1))
        s2 (set (keys p2))
        insec (clojure.set/intersection s1 s2)
        diff1 (clojure.set/difference s1 s2)
        diff2 (clojure.set/difference s2 s1)]
    (into {} (concat (map #(vector % (/ (+ (get p1 %) (get p2 %)) 2)) insec)
                     (map #(vector % (get p1 %)) diff1)
                     (map #(vector % (get p2 %)) diff2)))
    ))

(defn collect-location-chunks [profiles]
  (loop [remaining profiles
         current []
         acc []]
    (if (empty? remaining)
      (filter not-empty (conj acc current))
      (if (< (profile-strength (first remaining)) 0.6)
        (recur (rest remaining) [] (conj acc current))
        (recur (rest remaining) (conj current (first remaining)) acc)))))

(defn refine-location-chunk [chunk]
  (println "chunk has " (count chunk) "items")
  (loop [remaining (rest chunk)
         current [(first chunk)]
         acc []]
    (if (empty? remaining)
      (conj acc current)
      (if (< (profile-jaccard (first remaining) (last current)) 0.7)
        (recur (rest remaining) [(first remaining)] (conj acc current))
        (recur (rest remaining) (conj current (first remaining)) acc )))))

(defn get-p-in-profile [profile key]
  (let [result (get profile key)]
    (if (nil? result)
      0
      result)))

(defn get-p-in-profiles [profiles key]
  (zdiv (apply + (map #(get-p-in-profile % key) profiles)) (count profiles)))


(defn merge-chunk-profiles [chunk]
  (let [allkeys (apply clojure.set/union (map #(set (keys %)) chunk))]
    (zipmap allkeys (map #(get-p-in-profiles chunk %) allkeys))))

(defn find-unique-locations [candidates]
  (loop [remaining (rest candidates)
         locations [(first candidates)]]
    (if (empty? remaining)
      locations
      (let [similarities (map #(profile-jaccard % (first remaining)) locations)]
        (if (empty? (filter #(> % 0.4) similarities))
          (recur (rest remaining) (conj locations (first remaining)))
          (do
            (println "Already saw this position")
            (recur (rest remaining) locations)))))))


(defn find-sorted [db coll findby sortby]
  (with-collection db coll
    (find findby)
    (sort sortby)))


(defn update-db [db unique-locations]
  (let [raw  (vec (mc/find-maps db "wifiprofiles"))
        profiles (mapv :profile raw)
        all (map #(zipmap (map name (keys %)) (vals %)) profiles)]
    (loop [remaining unique-locations]
      (when-not (empty? remaining)
        (let [similarities (map #(profile-jaccard % (first remaining)) all)
              knownplaces (filter #(> % 0.4) similarities)
              placeindexes (map #(.indexOf similarities %) knownplaces)]
          (if (empty? knownplaces)
            (do (println "found new location --> update db")
                (mc/insert db "wifiprofiles" {:alias "unknown"
                                              :profile (first remaining)}))
            (do (println "i know this - it is one of" (map #(:alias (get raw %)) placeindexes))))
          (recur (rest remaining)))))))

(defn start [db]
  (let [all (find-sorted db "statedata" {} (array-map :time 1))
        chunks (make-chunks all 6)
        profiles (map make-wifi-profile chunks)
        dateranges (map #(str "  " (:time (first %)) " --> " (:time (last %))) chunks)
        strengths (map profile-strength profiles)]
    (pp/pprint (map str (map #(format "%.2f" %) (map profile-strength profiles)) dateranges))
    (println "compare adjacent profiles")
    (println (map #(if (< % 0.6) :TC :LC) strengths))
    (println (map profile-jaccard (butlast profiles) (rest profiles)))
    (let [location-chunks (filter #(> (count %) 1) (collect-location-chunks profiles))
          locations (flatten (map #(map merge-chunk-profiles %)
                                  (map refine-location-chunk location-chunks)))
          unique (find-unique-locations locations)]
      ;; (pp/pprint (map #(profile-jaccard (first %) (second %)) (comb/combinations unique 2) ))
      (update-db db unique)
      )))
