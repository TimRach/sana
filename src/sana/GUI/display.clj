;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns sana.GUI.display
    (:require [sana.state :as state]
              [hps.utilities.gui :as gut])
    (:import [javafx.scene.control CheckBox Label Tab TabPane]
             [javafx.scene.layout BorderPane GridPane VBox]))


(defn make-tab [name element]
  (let [tab (Tab. name)
        tab-content (BorderPane.)]
    (.setCenter tab-content element)
    (.setContent tab tab-content)
    tab))


(defn make-display []
  (let [tab-pane (TabPane.)]
    (.add (.getTabs tab-pane) (make-tab "Data Display 1" (VBox.)))
    (.add (.getTabs tab-pane) (make-tab "Data Display 2" (VBox.)))
    tab-pane))
