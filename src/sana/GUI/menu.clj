;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns sana.GUI.menu
    [:require
     [hps.utilities.gui :as gut]]
    (:import (javafx.scene.control MenuBar Menu MenuItem)
             (javafx.application Platform)
             (javafx.stage DirectoryChooser)
             (javafx.scene.input KeyCode KeyCodeCombination KeyCombination KeyCombination$Modifier)))


(defn adjust-menu-for-app [menubar scene]
  (let [menufile (first (filter #(= (.getText %) "File") (.getMenus menubar)))
        file-chooser (DirectoryChooser. )
        stage (.getWindow scene)]
    (gut/create-menu-item menufile
                          "Open Directory"
                          {:key "O" :modifier :ctrl}
                          (fn [] (let [directory (.showDialog file-chooser stage)]
                                  (when-not (nil? directory)
                                    (println "Function not implemented")
                                    (gut/adjust-stage-size (.getWindow scene))))))))
