(ns sana.GUI.graphview
  (:require [clojure.java.io :as io]
            [clojure.string :as string]
            [hps.decision-modules :refer :all]
            [hps.heuristics :refer :all]
            [hps.make-decision :as md]
            [hps.types :refer :all]
            [sana.state :as state]
            [clojure.pprint :as pp])
  (:import javafx.stage.Stage
           BackgroundCanvas
           [javafx.scene Scene]
           [javafx.scene.shape Rectangle]
           [javafx.scene.layout BorderPane]))


(defn getChain
  ([ob k1 k2] (-> ob (get (keyword k1)) (get (keyword k2)))))

;;Gets called by the graphview BackgroundCanvas component whenever a dm is created.
;;Has to check for existence because dms created by the user in the UI are not listed
;;at runtime
(defn registerDMWatch [dmname watcher]
  (println "Try to register watcher on " (keyword dmname))
  (when (get-dm-from-name (keyword dmname) 'sana.experts)
    (println "Register watch on " dmname "from " watcher)
    (add-watch (:internal-decision (get-dm-from-name (keyword dmname) 'sana.experts))
               :graphview (fn [k a o n] (.updateResult watcher n)))))

;;Gets called by the graphview BackgroundCanvas component whenever a statevar is added
;; to the state node.
(defn registerStateWatch [statevar watcher]
  (add-watch (get state/statevars (keyword statevar))
             :graphview (fn [k a o n] (.updateState watcher (apply list (map #(str % " : " @(get state/statevars %))
                                                                            (keys state/statevars)))))))

(defn deepconvert [hashmap]
  (if (= (type hashmap) java.util.HashMap)
    (let [toplevel (into {} hashmap)]
      (zipmap (map keyword (keys toplevel)) (map deepconvert (vals toplevel))))
    hashmap))

(defn read-all
  [input]
  (let [eof (Object.)]
    (take-while #(not= % eof) (repeatedly #(read input false eof)))))

(def expertscode
  (atom {:namespace nil
         :functions []
         :heuristics {}
         :decisionmodules {}}))


(defn exptype [exp]
  (let [kword (str (first exp))]
    (cond
      (= kword "ns") :Namespace
      (= kword "defn") :Function
      (= kword "def-heuristic") :Heuristic
      (= kword "declare-dm") :DecisionModule)))

(defn expname [exp]
  (keyword (second exp)))


(defrecord Internaldm [name aproducers aevaluators configurefun
                       onacceptancefun evalaggregatefun runfrequencyfun])

(defn parsedm [e]
  (let [dmname (second e)
        experts  (get (vec e) 3)
        aproducers (:producers experts)
        aevaluators (:evaluators experts)
        configurefun (get (vec e) 5)
        onacceptancefun (get (vec e) 7)
        evalaggregatefun (:evaluator-aggregation-fun (get (vec e) 9))
        runfrequencyfun (:run-frequency-fun (get (vec e) 9))]
    (Internaldm. dmname aproducers aevaluators configurefun
                 onacceptancefun evalaggregatefun runfrequencyfun)))


(defn parseexpertsfile []
  (swap! expertscode {:namespace nil :functions [] :heuristics {} :decisionmodules {}})
  (let [exps (read-string (str \( (slurp "src/sana/experts.clj")  \)))
        namespc (first exps)]
    (doseq [e exps]
      (cond (= (exptype e) :Namespace) (swap! expertscode assoc :namespace e))
      (cond (= (exptype e) :Function) (swap! expertscode update-in [:functions] conj e))
      (cond (= (exptype e) :Heuristic) (swap! expertscode update-in [:heuristics] assoc (expname e) e))
      (cond (= (exptype e) :DecisionModule)
            (swap! expertscode update-in [:decisionmodules] assoc (expname e) (parsedm e))
            ))))


(defn initializegraphview [graph dmns statens]
  (parseexpertsfile)
  (let [dms (list-decision-modules dmns)]
    (doseq [dm dms]
      (.addDecisionModule graph (get-dm-from-name dm 'sana.experts)))
    (doseq [stat (keys sana.state/statevars)]
      (.addStateVar graph stat))))


(defn header []
  (let [template (slurp "resources/templates/header.txt")
        nsstring (:namespace @expertscode)]
    (clojure.string/replace template #"@nsspec" (string/join " " nsstring))))


(defn fillheuristictemplate [name prodbody evalbody]
  (let [template (slurp "resources/templates/heuristic.txt")
        evalbodytemplate (slurp "resources/templates/evalbody.txt")
        prodbodytemplate (slurp "resources/templates/producerbody.txt")
        prod (if (nil? prodbody) nil (str prodbody))
        evl (if (nil? evalbody) nil (str evalbody))]
    (-> template
        (string/replace #"@prodbody" (or prod prodbodytemplate))
        (string/replace #"@evalbody" (or evl evalbodytemplate))
        (string/replace #"@hname" (str name)))))

(defn heuristicbodies [h]
  (let [body (vec (get (:heuristics @expertscode) h))]
    [(get body 3) (get body 5)]))

(defn updateheuristicentries [dms]
  (let [heutemplate (slurp "resources/templates/heuristic.txt")
        producernames (flatten (map #(keys (:producers (second %))) dms))
        evaluatornames (flatten (map #(keys (:evaluators (second %))) dms))
        allnames (vec (clojure.set/union (set producernames) (set evaluatornames)))]
    (doseq [n allnames]
      (if(contains? (:heuristics @expertscode) n)
        (swap! expertscode update-in [:heuristics]
               assoc n (fillheuristictemplate (str n)
                                              (first (heuristicbodies n))
                                              (second (heuristicbodies n))))
        (swap! expertscode update-in [:heuristics]
               assoc n (fillheuristictemplate (str n) nil nil))))))

(defn makedmentry [dm]
  (let [dmtemplate (slurp "resources/templates/decisionmodule.txt")]
    (->
     dmtemplate
     (string/replace #"@dmname" (str (:name dm)))
     (string/replace #"@producers" (str (:aproducers dm)))
     (string/replace #"@evaluators" (str (:aevaluators dm)))
     (string/replace #"@evaluator-aggregation-fun" (str (or (:evalaggregatefun dm)
                                                            ":weighted-sum")))
     (string/replace #"@onacceptance-fun" (str (or (:onacceptancefun dm)
                                                   "(fn [dec] nil)"))))))


(defn updateexpertscode [decisionmodules]
  (doseq [dm decisionmodules]
    (let [newname (first dm)
          oname (keyword (:originalname (second dm)))
          internalDM (or (get (:decisionmodules @expertscode) oname)
                         (Internaldm. newname nil nil nil nil nil nil))]
      (println newname)
      (swap! expertscode update-in [:decisionmodules] assoc oname
             (-> internalDM
                 (assoc-in [:name] newname)
                 (assoc-in [:aproducers] (vec (keys (:producers (second dm)))))
                 (assoc-in [:aevaluators] (vec (keys (:evaluators (second dm)))))))))
  (updateheuristicentries decisionmodules))



(defn reloadexperts []
  (load-file "src/sana/experts.clj"))


(defn saveConfiguration [config]
  (let [cconfig (deepconvert config)
        dms (:decisionmodules cconfig)]
    (updateexpertscode dms)
    (spit "src/sana/experts.clj" (header))
    (spit "src/sana/experts.clj" (str "\n\n" (string/join "\n\n" (:functions @expertscode))) :append true)
    (spit "src/sana/experts.clj" (string/join "\n\n" (vals (:heuristics @expertscode))) :append true)
    (spit "src/sana/experts.clj" (apply str (map makedmentry (vals (:decisionmodules @expertscode)))) :append true))
  (parseexpertsfile)
  (reloadexperts))
