;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns sana.GUI.controls
    (:require [hps.GUI.make-decision :refer :all]
              [hps.decision-modules :as dms]
              [hps.utilities.gui :as gut]
              [sana.state :as state]
              [sana.experts :as experts])
    (:import (javafx.scene.control Slider Button)
             (javafx.scene.layout HBox VBox)
             (javafx.beans.value ChangeListener)))

(defmacro add-listener [property action]
  `(.addListener ~property
                 (reify ChangeListener
                   (changed [this obsval oldval newval]
                     ~action))))


(defn make-control-buttons [scene]
  (let [run-button-box (HBox.)
        button-run (Button. "Start DMs")
        button-pause (Button. "Pause DMs")
        button-next (Button. ">")
        button-prev (Button. "<")]
    (gut/add-to-container run-button-box button-prev button-run button-pause button-next)
    (gut/set-button-action button-prev (state/on-step-back))
    (gut/set-button-action button-next (state/on-step-fwd))
    (gut/set-button-action button-run (state/on-run))
    (gut/set-button-action button-pause (state/on-pause))
    run-button-box))


(defn make-control-slider []
  (let [slider (Slider.)]
    (.setMin slider 0)
    (.setMax slider 100)
    (.setMajorTickUnit slider 1)
    (.setSnapToTicks slider true)
    (.setValue slider 0)
    (add-listener (.valueProperty slider) (state/on-slider-changed (int (.getValue slider))))
    slider))

(defn make-control-elements [scene]
  (let [ctrl-wrap (VBox.)
        buttons (make-control-buttons scene)
        slider (make-control-slider)]
    (gut/add-to-container ctrl-wrap slider buttons)
    ctrl-wrap))
