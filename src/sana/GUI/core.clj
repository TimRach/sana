;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns sana.GUI.core
    (:require [clojure.java.io :as io]
              [hps.GUI.structure :as structure]
              sana.GUI.controls
              sana.GUI.display
              sana.GUI.menu
              sana.GUI.graphview
              [sana.state :as state]
              [hps.decision-modules :as dm]
              [hps.utilities.gui :as gut]
              [sana.experts :as experts])
    (:import [javafx.event Event EventHandler]
             [javafx.scene.control Tab TabPane]
             [javafx.scene.layout BorderPane HBox Priority]))

(defn load-dm-configuration [dm-name]
  (get (dm/list-decision-modules) (keyword dm-name)))


(defn initialize-expert-display [hps-displays dm-keyword]
  (let [experts-pane (some #(when (= (.getId %) "hps-configbox") %) (.getItems hps-displays))
        {:keys [producers evaluators adaptors] :as experts} (:available-experts (dm/get-dm-from-name dm-keyword 'sana.experts))
        [producer-pane adaptor-pane evaluator-pane parameters-pane :as panes] (vec (.getChildren experts-pane))]
    (run! (fn [[pane experts]]
            (hps.GUI.make-decision/initialize-experts-in-display pane experts)
            (.setSelected (some #(when (= (.getText %) "All") %) (.getChildren (.getContent pane))) true))
          [[producer-pane producers] [evaluator-pane evaluators] [adaptor-pane adaptors]])))


(defn make-decision-module-tabs [tab-pane decision-modules]
  (doseq [dm-keyword decision-modules]
    (let [tab (Tab. (name dm-keyword))
          tab-content-pane (BorderPane.)
          tabcontentpane-hps-displays (structure/make-decision-module-basic-display)
          display-parameters (dissoc hps.make-decision/produce-and-evaluate-default-params :evaluator-aggregation-fun)]
      (initialize-expert-display tabcontentpane-hps-displays dm-keyword)
      (.setId tabcontentpane-hps-displays (str "dm-pane-tab-" (name dm-keyword)))
      (.setDividerPosition tabcontentpane-hps-displays 0 0.4)
      (.add (.getStyleClass tab-content-pane) "morph-tabpane-with-goal")
      (.setStyle tab (str "-fx-background-color: #aaa;"))
      (.setStyle tab-content-pane (str "-fx-background-color: #aaa;"))
      (.setCenter tab-content-pane tabcontentpane-hps-displays)
      (.setContent tab tab-content-pane)
      (.add (.getTabs tab-pane) tab)
      (.setOnSelectionChanged tab
                              (proxy [EventHandler] []
                                (handle [^Event event]
                                  (when (.isSelected tab)
                                    (load-dm-configuration(.getText tab)))))))))


(defn make-graphview-tab [scene]
  (let [tab (Tab. "Graphview")
        container (BackgroundCanvas. 100 100)]
    (.setScene container scene)
    (.setContent tab container)
    (sana.GUI.graphview/initializegraphview container 'sana.experts 'sana.state)
    tab))


(defn display-decision-process [scene]
  (let [hps-split-pane (.lookup scene "#hps-centerSplitPane")
        controlRegion  (.lookup scene "#hps-controlRegion")
        [decisionModules stateRegion] (vec (.getItems hps-split-pane))
        decision-modules-tab-pane (TabPane.)
        graphtab (make-graphview-tab scene)
        info-and-control-box (sana.GUI.controls/make-control-elements scene)]

    (.setId decision-modules-tab-pane "dm-tabpane")
    (gut/add-to-container controlRegion info-and-control-box)
    (gut/add-to-container decisionModules decision-modules-tab-pane)

    (HBox/setHgrow decision-modules-tab-pane Priority/ALWAYS)
    (.setDividerPosition hps-split-pane 0 0.6)

    ;; (make-decision-module-tabs decision-modules-tab-pane
    ;;                            (dm/list-decision-modules 'sana.experts))
    (.add (.getTabs decision-modules-tab-pane) graphtab)
    (.setId (.getContent graphtab) "graph-view-container")
    (gut/add-to-container stateRegion (sana.GUI.display/make-display))))


(defn adjust-gui-for-app [scene]
  (.add (.getStylesheets scene) (.toExternalForm (io/resource "css/sana.css")))
  (.add (.getStylesheets scene) (.toExternalForm (io/resource "css/graphview.css")))
  (sana.GUI.menu/adjust-menu-for-app (.lookup scene "#hps-menubar-left") scene)
  (display-decision-process scene))


(hps.GUI.structure/register-hps-mode "sana" adjust-gui-for-app true)
