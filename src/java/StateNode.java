import clojure.lang.Range;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.CubicCurve;
import javafx.scene.text.TextAlignment;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class StateNode extends StackPane{

    private Label title;
    HBox headerBox = new HBox();

    private ListView<String> paramslist = new ListView<String>();
    public ObservableList<String> items = FXCollections.observableArrayList();


    public StateNode(String _title, NodeType type, Object parameters) {
    }

    public StateNode(String _title, ArrayList<String> parameters) {
        if(parameters != null){
            for(int i = 0; i < parameters.size(); i++){
                items.add(parameters.get(i));
            }
        }

        String stylePrefix = "input";
        //Make TitlePane
        headerBox.setMinWidth(200);
        Label typeChar = new Label("-");
        typeChar.setTranslateX(7);
        typeChar.setTranslateY(2);
        typeChar.getStyleClass().add(stylePrefix + "-type");

        title = new Label(_title);
        title.setTextAlignment(TextAlignment.CENTER);
        title.setTranslateX(25);
        title.setTranslateY(2);
        title.getStyleClass().add(stylePrefix + "-title");

        headerBox.getChildren().add(typeChar);
        headerBox.getChildren().add(title);

        //Make ContentPane
        VBox contentPane = new VBox();
        contentPane.setTranslateY(20);

        // HBox buttonbar = new HBox();
        // Button addEntry = new Button("+ State Var");
        // addEntry.getStyleClass().add("add-button");
        // addEntry.setOnAction(addEntryEvent);

        paramslist.setItems(items);
        paramslist.setPrefWidth(200);
        paramslist.setEditable(true);
        // paramslist.setCellFactory(TextFieldListCell.forListView());
        paramslist.setCellFactory(param -> new EDListCell());
        contentPane.getChildren().add(paramslist);
        // buttonbar.getChildren().add(addEntry);
        // contentPane.getChildren().add(buttonbar);

        headerBox.getStyleClass().add(stylePrefix + "-header");
        contentPane.getStyleClass().add(stylePrefix + "-content");


        getChildren().add(headerBox);
        getChildren().add(contentPane);
    }

    EventHandler<ActionEvent> addEntryEvent = new EventHandler<ActionEvent>(){
        @Override public void handle(ActionEvent e) {
            items.add("New Item");
        }
    };

    public void updateState(clojure.lang.PersistentList newVals){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                items.clear();
                items.addAll(newVals);
            }
        });
    }


    public int getNodeHeight(){
        return (int)getHeight();
    }

    private boolean isRendered = false;
    protected void layoutChildren() {
        super.layoutChildren();
        if(!isRendered){
        }
        isRendered = true;
    }

    public double getStartX(){
        return getTranslateX();
    }
    public double getEndX(){
        return getTranslateX() + getWidth();
    }
    public double getCenterX(){
        return getTranslateX() + (getWidth() * 0.5);
    }
    public double getCenterY(){
        return getTranslateY() + (getHeight() * 0.5);
    }
    public double getStartY(){
        return getTranslateY();
    }
    public double getEndY(){
        return getTranslateY() + getHeight();
    }


}
