import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.CubicCurve;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.StrokeLineCap;

import clojure.lang.Keyword;
import clojure.lang.PersistentArrayMap;
/**
 * A node that draws a triangle grid of dots using canvas
 */
public class DotGrid extends Pane {

    public final Canvas lineholder = new Canvas(4000,4000);
    DoubleProperty myScale = new SimpleDoubleProperty(1.0);

    public BackgroundCanvas parent;
    private static final double OFFSET = 100;

    private HashMap<String,DecisionModule> decisionModules;
    private HashMap<String,GraphNode> otherNodes;
    public DotGrid() {
        getChildren().add(lineholder);
        decisionModules = new HashMap<String, DecisionModule>();
        otherNodes = new HashMap<String,GraphNode>();
        setPrefSize(4000,4000);
    }

    public double getScale() {
        return myScale.get();
    }

    public void setScale( double scale) {
        myScale.set(scale);
    }

    public void setPivot( double x, double y) {
        setTranslateX(getTranslateX()-x);
        setTranslateY(getTranslateY()-y);
    }

    public GraphNode addGenericNode (String name, ArrayList<String> fields){
        GraphNode node = new GraphNode(name, NodeType.GENERIC, fields);
        otherNodes.put(name, node);
        getChildren().add(node);
        return node;
    }

    public DecisionModule addDecisionModule(hps.types.DecisionModule _dm){
        DecisionModule decisionModule = null;
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            VBox root = fxmlLoader.load(getClass().getResource("decision_module.fxml").openStream());
            decisionModule = fxmlLoader.getController();
            if(_dm != null) {
                decisionModule.setName(((Keyword) _dm.get(Keyword.intern("name"))).getName());
                PersistentArrayMap experts = (PersistentArrayMap) _dm.get(Keyword.intern("available-experts"));
                Object[] producers = ((clojure.lang.LazySeq) experts.get(Keyword.intern("producers"))).toArray();
                Object[] evaluators = ((clojure.lang.LazySeq) experts.get(Keyword.intern("evaluators"))).toArray();
                for (Object o : producers) {
                    decisionModule.AddProducer((hps.types.Expert) o);
                }
                for (Object o : evaluators) {
                    decisionModule.AddEvaluator((hps.types.Expert) o);
                }
                decisionModule.setDMConfig(BackgroundCanvas.getChain.invoke(_dm, "decision-parameters", "evaluator-aggregation-fun"));
            }else{
                decisionModule.setName("NEWDM");
            }

            getChildren().add(root);
            decisionModules.put(decisionModule.name, decisionModule);
        }catch (Exception e){
            e.printStackTrace();
        }
        return decisionModule;
    }

    private boolean isRendered = false;
    protected void layoutChildren() {
        super.layoutChildren();
       /* if(!isRendered){
            ArrayList<DecisionModule>  remaining = new ArrayList<DecisionModule>();
            ArrayList<Tuple> dockpoints = new ArrayList<Tuple>();

            dockpoints.add(new Tuple<Double,Double>(0.0,0.0));
            for(String key: decisionModules.keySet()){
                remaining.add(decisionModules.get(key));
            }

            int layouted = 0;
            int j = 0;
            while(layouted < remaining.size()){
                    DecisionModule candidate = remaining.get(j);
                    j = (j + 1) % remaining.size();
                    if(candidate.wasAutoLayouted){
                        continue;
                    }
                    for(int i = 1; i < remaining.size(); i++){
                        if(remaining.get(i).outConnections.size() > candidate.outConnections.size()){
                            candidate = remaining.get(i);
                        }
                    }
                    candidate.wasAutoLayouted = true;
                    layouted++;
                    candidate.setTranslateX((double)((Tuple)dockpoints.get(0)).x);
                    candidate.setTranslateY((double)((Tuple)dockpoints.get(0)).y);
                    // System.out.println("Positioned candidate at" + candidate.getTranslateX() + " - " +
                    //                    candidate.getTranslateY() + " with w/h"
                    //                    + candidate.getWidth() + "/" + candidate.getHeight());
                    dockpoints.add(new Tuple<Double,Double>(candidate.getTranslateX() + candidate.getWidth() + OFFSET,
                                                            candidate.getTranslateY()));
                    dockpoints.add(new Tuple<Double,Double>(candidate.getTranslateX(),
                                                            candidate.getTranslateY() + candidate.getHeight() + OFFSET));
                dockpoints.remove(dockpoints.get(0));
            }
        }*/
        isRendered = true;
    }

    public static CubicCurve curvedConnection(double startx, double starty, double endx, double endy){
        CubicCurve curve = new CubicCurve();
        curve.setStartX(startx);
        curve.setStartY(starty);
        curve.setControlX1(startx+150);
        curve.setControlY1(starty);
        curve.setControlX2(endx-150);
        curve.setControlY2(endy);
        curve.setEndX(endx);
        curve.setEndY(endy);
        curve.setStroke(Color.YELLOW);
        curve.setStrokeWidth(4);
        curve.setStrokeLineCap(StrokeLineCap.ROUND);
        curve.setFill(Color.CORNSILK.deriveColor(0, 1.2, 1, 0));
        return curve;
    }


    public void connectDecisionModules(DecisionModule from, DecisionModule to){
        // layoutChildren();
        // CubicCurve connLine = curvedConnection(from.getEndX(), from.getCenterY(),
        //                                        to.getStartX(), to.getCenterY());
        CubicCurve connLine = curvedConnection(0,0,0,0);
        // System.out.println("Added connection from " +from.getEndX() +  "," + from.getCenterY()
        //                    + " to " + to.getStartX() + "," + to.getCenterY());
  //      from.addOutConnection(connLine);
//        to.addInConnection(connLine);

        getChildren().add(connLine);
    }


    public class Tuple<X, Y> {
        public final X x;
        public final Y y;
        public Tuple(X x, Y y) {
            this.x = x;
            this.y = y;
        }
    }
}
