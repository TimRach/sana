
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;
import java.io.PrintWriter;

import clojure.java.api.Clojure;
import clojure.lang.IFn;
import javafx.scene.shape.Line;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.paint.Color;
import javafx.scene.layout.Pane;
import javafx.scene.control.Button;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import hps.types.DecisionResult;
import javafx.stage.FileChooser;


public class BackgroundCanvas extends Pane{

    Scene myScene;
    SceneGestures gestures;
    public NodeGestures nodeGestures;
    DotGrid content;
    HashMap<String,DecisionModule> decisionModules;
    StateNode state;
    HashMap<String,GraphNode> otherNodes;

    Button saveButton = new Button("Save");
    Button loadButton = new Button("Load");
    Button addDMButton = new Button("+ DM");

    public BackgroundCanvas (int width, int height){
        setMinWidth(width);
        setMinHeight(height);
        getStyleClass().add("background-canvas");
        content = new DotGrid();
        content.parent = this;

        content.setMinWidth(width);
        content.setMinHeight(height);

        getChildren().add(content);
        gestures = new SceneGestures(content);
        nodeGestures = new NodeGestures(content);
        decisionModules = new HashMap<String, DecisionModule>();
        otherNodes = new HashMap<String, GraphNode>();

        saveButton.setOnAction(savePositionsEvent);
        loadButton.setOnAction(loadPositionsEvent);
        addDMButton.setOnAction(addDMEvent);

        state = new StateNode("STATE",null);

        content.getChildren().add(state);
        getChildren().addAll(addDMButton, saveButton, loadButton);
        loadButton.fire();
    }


    IFn saveConfiguration = Clojure.var("sana.GUI.graphview", "saveConfiguration");
    IFn registerDMWatch = Clojure.var("sana.GUI.graphview", "registerDMWatch");
    IFn registerStateWatch = Clojure.var("sana.GUI.graphview", "registerStateWatch");
    public static IFn getChain = Clojure.var("sana.GUI.graphview", "getChain");

    EventHandler<ActionEvent> addDMEvent = new EventHandler<ActionEvent>(){
            @Override public void handle(ActionEvent e) {
                addDecisionModule(null);
            }
        };

    EventHandler<ActionEvent> savePositionsEvent = new EventHandler<ActionEvent>(){
            @Override public void handle(ActionEvent e) {
                try{
                    PrintWriter writer = new PrintWriter(".graphpositions.txt", "UTF-8");
                    for(String key: decisionModules.keySet()){
                        DecisionModule dm = decisionModules.get(key);
                        writer.println(key + "," + (int)dm.root.getTranslateX() + "," + (int)dm.root.getTranslateY());
                    }
                    writer.println("");
                    for(String key: otherNodes.keySet()){
                        GraphNode node = otherNodes.get(key);
                        writer.println(key + "," + (int)node.getTranslateX() + "," + (int)node.getTranslateY());
                    }
                    writer.close();

                    saveConfiguration.invoke(clojureRep());
                }catch(Exception ex){
                    System.out.println(ex);
                }
            }
        };



    public HashMap<String,HashMap> clojureRep (){
        HashMap<String, HashMap> result = new HashMap<>();
        HashMap<String, HashMap> dms = new HashMap<>();
        for(DecisionModule dm : decisionModules.values()){
            //dm.clojureRep() needs to be executed first to assure the most recent dm.name
            HashMap cljrep = dm.clojureRep();
            dms.put(dm.name, cljrep);
        }
        result.put("decisionmodules", dms);
        result.put("state", null);
        return result;
    }


    EventHandler<ActionEvent> loadPositionsEvent = new EventHandler<ActionEvent>(){
                @Override public void handle(ActionEvent e){
                    try{
                        Scanner scanner = new Scanner(new File(".graphpositions.txt"));
                        scanner.useDelimiter("\n");
                        while(scanner.hasNext()){
                           String[] values = scanner.next().split(",");
                            if(values.length < 3) break;
                            String key = values[0];
                            System.out.println("Set posoition for " + key);
                            float x = Float.parseFloat(values[1]);
                            float y = Float.parseFloat(values[2]);
                            if(decisionModules.get(key) != null){
                                decisionModules.get(key).root.setTranslateX(x);
                                decisionModules.get(key).root.setTranslateY(y);
                            }
                        }
                        while(scanner.hasNext()){
                            String[] values = scanner.next().split(",");
                            String key = values[0];
                            float x = Float.parseFloat(values[1]);
                            float y = Float.parseFloat(values[2]);
                            if(otherNodes.get(key) != null){
                                System.out.println("Set other node posoition");
                                otherNodes.get(key).setTranslateX(x);
                                otherNodes.get(key).setTranslateY(y);
                            }
                        }
                        scanner.close();
                    }catch(Exception ex){
                        System.out.println(ex);
                    }
                    System.out.println("Loaded graph node layout");
                }
            };

    public void addGenericNode (String name, ArrayList<String> fields){
        System.out.println(" Add " + name + " with " + fields);
        GraphNode node = content.addGenericNode(name, fields);
        otherNodes.put(name, node);

        node.headerBox.addEventFilter(MouseEvent.MOUSE_PRESSED, nodeGestures.getOnMousePressedEventHandler());
        node.headerBox.addEventFilter(MouseEvent.MOUSE_DRAGGED, nodeGestures.getOnMouseDraggedEventHandler());
    }

    public void addDecisionModule(hps.types.DecisionModule dm){
        DecisionModule mod = content.addDecisionModule(dm);
        decisionModules.put(mod.name,mod);
        registerDMWatch.invoke(mod.name, mod);

        mod.headerBox.addEventFilter(MouseEvent.MOUSE_PRESSED, nodeGestures.getOnMousePressedEventHandler());
        mod.headerBox.addEventFilter(MouseEvent.MOUSE_DRAGGED, nodeGestures.getOnMouseDraggedEventHandler());
    }


    public void addStateVar(clojure.lang.Keyword statevar){
        state.items.add(statevar.toString());
        registerStateWatch.invoke(statevar, state);
        state.headerBox.addEventFilter(MouseEvent.MOUSE_PRESSED, nodeGestures.getOnMousePressedEventHandler());
        state.headerBox.addEventFilter(MouseEvent.MOUSE_DRAGGED, nodeGestures.getOnMouseDraggedEventHandler());
    }


    public void setScene(Scene scene){
        myScene = scene;
        myScene.addEventFilter(MouseEvent.MOUSE_PRESSED, gestures.getOnMousePressedEventHandler());
        myScene.addEventFilter(MouseEvent.MOUSE_DRAGGED, gestures.getOnMouseDraggedEventHandler());
        content.lineholder.addEventFilter(ScrollEvent.ANY, gestures.getOnScrollEventHandler());
    }

    private boolean isRendered = false;
    protected void layoutChildren() {
        super.layoutChildren();
        if(!isRendered){
            for(String key: decisionModules.keySet()){
                //decisionModules.get(key).layoutChildren();
            }
        }
        loadButton.setTranslateY(getHeight() - 30);
        saveButton.setTranslateX(getWidth() - 50);
        saveButton.setTranslateY(getHeight() - 30);
    }
}
