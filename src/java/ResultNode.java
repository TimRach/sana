
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Point2D;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.TextAlignment;

import clojure.lang.Keyword;

import hps.types.DecisionAlternative;

public class ResultNode extends StackPane {
    private Label title;
    private ComboBox<String> alternatives;

    double orgSceneX, orgSceneY;
    double orgTranslateX, orgTranslateY;

    public ResultNode(String _title, NodeType type) {

        Pane titlePane = new Pane();
        titlePane.setMinWidth(175);

        Pane contentPane = new Pane();
        contentPane.setMinHeight(50);
        contentPane.setTranslateY(20);

        Label typeChar = new Label("-");
        typeChar.setText("R");
        typeChar.setTranslateX(7);
        typeChar.setTranslateY(2);
        typeChar.getStyleClass().add("expert-type");


        title = new Label(_title);
        title.setTextFill(Color.BLACK);
        title.setTextAlignment(TextAlignment.CENTER);
        title.setTranslateX(25);
        title.setTranslateY(2);
        title.getStyleClass().add("expert-title");

        titlePane.getChildren().add(typeChar);
        titlePane.getChildren().add(title);


        makeDataField(contentPane);

        titlePane.getStyleClass().add("expert-header");
        contentPane.getStyleClass().add("expert-content");

        getChildren().add(titlePane);
        getChildren().add(contentPane);
    }


    public void makeDataField(Pane contentPane){

        ObservableList<String> altlist = FXCollections.observableArrayList();
        alternatives = new ComboBox<String>(altlist);
        alternatives.setTranslateX(5);
        alternatives.setTranslateY(5);
        // alternatives.getItems().addAll("Alternative 1: 0.95","Alternative 2: 0.77", "Alternative 3: 0.01");
        // alternatives.getSelectionModel().selectFirst();
        contentPane.getChildren().add(alternatives);
    }

    public void updateDataField(DecisionAlternative decAlt){
        if(decAlt == null)
            return;
        Object id = decAlt.get(Keyword.intern("value"));
        Double evaluation = (Double)decAlt.get(Keyword.intern("aggregated-evaluation"));
        alternatives.getItems().clear();
        alternatives.getItems().add(id + ": " + GraphNode.internalToString(evaluation));
        alternatives.getSelectionModel().selectFirst();
    }

}
