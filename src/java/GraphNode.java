import clojure.lang.Range;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.CubicCurve;
import javafx.scene.text.TextAlignment;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;


public class GraphNode extends StackPane implements IConnectable {

    private Label title;
    HBox headerBox = new HBox();

    private ListView<String> paramslist = new ListView<String>();
    private ObservableList<String> items = FXCollections.observableArrayList();

    public ArrayList<CubicCurve> inConnections = new ArrayList<CubicCurve>();
    public ArrayList<CubicCurve> outConnections = new ArrayList<CubicCurve>();


    public GraphNode(String _title, NodeType type, Object parameters) {
    }

    public GraphNode(String _title, NodeType type, ArrayList<String> parameters) {
        if(parameters != null){
            for(int i = 0; i < parameters.size(); i++){
                items.add(parameters.get(i));
            }
        }

        String stylePrefix = "graphnode";
        //Make TitlePane
        headerBox.setMinWidth(200);
        Label typeChar = new Label("-");
        switch (type) {
        case PRODUCER: typeChar.setText("P"); stylePrefix = "expert";break;
        case EVALUATOR: typeChar.setText("E"); stylePrefix = "expert";break;
        case GENERIC: typeChar.setText(""); stylePrefix = "input";break;
        }
        typeChar.setTranslateX(7);
        typeChar.setTranslateY(2);
        typeChar.getStyleClass().add(stylePrefix + "-type");

        title = new Label(_title);
        title.setTextFill(Color.BLACK);
        title.setTextAlignment(TextAlignment.CENTER);
        title.setTranslateX(25);
        title.setTranslateY(2);
        title.getStyleClass().add(stylePrefix + "-title");

        headerBox.getChildren().add(typeChar);
        headerBox.getChildren().add(title);

        //Make ContentPane
        VBox contentPane = new VBox();
        contentPane.setTranslateY(20);

        HBox buttonbar = new HBox();
        Button addEntry = new Button("+");
        addEntry.getStyleClass().add("add-button");
        addEntry.setOnAction(addEntryEvent);

        paramslist.setItems(items);
        paramslist.setPrefWidth(200);
        paramslist.setEditable(true);
        // paramslist.setCellFactory(TextFieldListCell.forListView());
        paramslist.setCellFactory(param -> new EDListCell());
        contentPane.getChildren().add(paramslist);
        buttonbar.getChildren().add(addEntry);
        contentPane.getChildren().add(buttonbar);

        headerBox.getStyleClass().add(stylePrefix + "-header");
        contentPane.getStyleClass().add(stylePrefix + "-content");


        getChildren().add(headerBox);
        getChildren().add(contentPane);
    }

    EventHandler<ActionEvent> addEntryEvent = new EventHandler<ActionEvent>(){
            @Override public void handle(ActionEvent e) {
                items.add("New Item");
            }
        };

    public static String internalToString(Object ivalue){
        DecimalFormat df = new DecimalFormat("#.##");
        if(ivalue instanceof java.lang.Long){
            return df.format((Long) ivalue);
        }else if(ivalue instanceof java.lang.Double){
            return df.format((Double) ivalue);
        }else if(ivalue instanceof Range){
            String str = "(";
            Iterator it = ((Range) ivalue).iterator();
            while(it.hasNext()){
                str += internalToString(it.next()) + ",";
            }
            return str.substring(0,str.length()-1) + ")";
        }
        else if(ivalue instanceof clojure.lang.LazySeq){
            String str = "(";
            Iterator it = ((clojure.lang.LazySeq) ivalue).iterator();
            while(it.hasNext()){
                str += internalToString(it.next()) + ",";
            }
            return str.substring(0,str.length()-1) + ")";
        }
        else{
            return ivalue.toString();
        }
    }

    public int getNodeHeight(){
        return (int)getHeight();
    }

    private boolean isRendered = false;
    protected void layoutChildren() {
        super.layoutChildren();
        if(!isRendered){
        }
        isRendered = true;
    }

    public double getStartX(){
        return getTranslateX();
    }
    public double getEndX(){
        return getTranslateX() + getWidth();
    }
    public double getCenterX(){
        return getTranslateX() + (getWidth() * 0.5);
    }
    public double getCenterY(){
        return getTranslateY() + (getHeight() * 0.5);
    }
    public double getStartY(){
        return getTranslateY();
    }
    public double getEndY(){
        return getTranslateY() + getHeight();
    }



    public void updateConnections(){
        for(CubicCurve l: inConnections){
                l.setEndX(getCenterX());
                l.setEndY(getStartY());
                l.setControlX2(getCenterX());
                l.setControlY2(getStartY()-150);
            }
        for(CubicCurve ol: outConnections){
            ol.setStartX(getCenterX());
            ol.setStartY(getEndY());
            ol.setControlX1(getCenterX());
            ol.setControlY1(getEndY()+150);
        }
    }



}
