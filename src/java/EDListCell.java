import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.util.converter.DefaultStringConverter;

public class EDListCell extends TextFieldListCell<String> {
    public static int prefHeight = 30;

 //   CheckBox checkBox = new CheckBox("");
    HBox hbox = new HBox();
    Label label = new Label("");
    Pane pane = new Pane();
    Button button = new Button("-");

    public EDListCell() {
        super();
        setConverter(new DefaultStringConverter());

        hbox.getChildren().addAll(label, pane, button);
        hbox.setAlignment(Pos.CENTER_LEFT);
        hbox.setPrefHeight(prefHeight);
        HBox.setHgrow(pane, Priority.ALWAYS);
        button.getStyleClass().add("remove-button");
        button.setOnAction(event -> getListView().getItems().remove(getItem()));
    }

    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
        setText(null);
        setGraphic(null);

        if (item != null && !empty) {
            //checkBox.setText(item);
            label.setText(item);
            setGraphic(hbox);
        }
    }
}