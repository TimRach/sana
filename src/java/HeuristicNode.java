import java.util.HashMap;
import java.util.Iterator;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Point2D;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.shape.CubicCurve;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.control.Button;
import javafx.scene.text.TextAlignment;
import javafx.event.ActionEvent;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.event.EventHandler;

import clojure.lang.PersistentArrayMap;
import clojure.lang.Keyword;
import clojure.lang.Range;

public class HeuristicNode extends StackPane implements IConnectable {

    private Label title;

    public HeuristicNode(String _title, NodeType type) {
        String stylePrefix = "heuristic";
        Label typeChar = new Label("H");
        Pane titlePane = new Pane();

        titlePane.setMinWidth(200);
        switch (type) {
        case PRODUCER: typeChar.setText("P"); stylePrefix = "expert";break;
        case EVALUATOR: typeChar.setText("E"); stylePrefix = "expert";break;
        case GENERIC: typeChar.setText(""); stylePrefix = "input";break;
        }
        typeChar.setTranslateX(7);
        typeChar.setTranslateY(2);
        typeChar.getStyleClass().add(stylePrefix + "-type");

        title = new Label(_title);
        title.setTextFill(Color.BLACK);
        title.setTextAlignment(TextAlignment.CENTER);
        title.setTranslateX(25);
        title.setTranslateY(2);
        title.getStyleClass().add(stylePrefix + "-title");

        titlePane.getChildren().add(typeChar);
        titlePane.getChildren().add(title);

        //Make ContentPane
        VBox contentPane = new VBox();
        contentPane.setTranslateY(20);

        HBox buttonbar = new HBox();
        Button addEntry = new Button("+P");
        Button removeEntry = new Button("-P");
        addEntry.setOnAction(addEntryEvent);
        removeEntry.setOnAction(removeEntryEvent);
        buttonbar.getChildren().add(addEntry);
        buttonbar.getChildren().add(removeEntry);
        contentPane.getChildren().add(buttonbar);

        titlePane.getStyleClass().add(stylePrefix + "-header");
        contentPane.getStyleClass().add(stylePrefix + "-content");
        getChildren().add(titlePane);
        getChildren().add(contentPane);
    }

    EventHandler<ActionEvent> addEntryEvent = new EventHandler<ActionEvent>(){
            @Override public void handle(ActionEvent e) {
            }
        };
    EventHandler<ActionEvent> removeEntryEvent = new EventHandler<ActionEvent>(){
            @Override public void handle(ActionEvent e) {
                        }
        };

    public static String internalToString(Object ivalue){
        DecimalFormat df = new DecimalFormat("#.##");
        if(ivalue instanceof java.lang.Long){
            return df.format((Long) ivalue);
        }else if(ivalue instanceof java.lang.Double){
            return df.format((Double) ivalue);
        }else if(ivalue instanceof Range){
            String str = "(";
            Iterator it = ((Range) ivalue).iterator();
            while(it.hasNext()){
                str += internalToString(it.next()) + ",";
            }
            return str.substring(0,str.length()-1) + ")";
        }
        else if(ivalue instanceof clojure.lang.LazySeq){
            String str = "(";
            Iterator it = ((clojure.lang.LazySeq) ivalue).iterator();
            while(it.hasNext()){
                str += internalToString(it.next()) + ",";
            }
            return str.substring(0,str.length()-1) + ")";
        }
        else{
            return ivalue.toString();
        }
    }

    public int getNodeHeight(){
        return (int)getHeight();
    }

    private boolean isRendered = false;
    protected void layoutChildren() {
        super.layoutChildren();
        if(!isRendered){
        }
        isRendered = true;
    }

    public double getStartX(){
        return getTranslateX();
    }
    public double getEndX(){
        return getTranslateX() + getWidth();
    }
    public double getCenterX(){
        return getTranslateX() + (getWidth() * 0.5);
    }
    public double getCenterY(){
        return getTranslateY() + (getHeight() * 0.5);
    }
    public double getStartY(){
        return getTranslateY();
    }
    public double getEndY(){
        return getTranslateY() + getHeight();
    }



    public void updateConnections(){

    }

}
