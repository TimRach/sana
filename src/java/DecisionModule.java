import clojure.java.api.Clojure;
import clojure.lang.IFn;
import clojure.lang.Keyword;
import hps.types.DecisionAlternative;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.HashMap;

public class DecisionModule {

    @FXML public VBox root;
    @FXML public HBox headerBox;
    @FXML TextField titleText;
    @FXML Label resultLabel;
    @FXML Label evaluatoraggregationfun;
    @FXML ListView<String> listProd;
    @FXML ListView<String> listEval;
    @FXML ListView<String> listResults;
    @FXML ObservableList<String> producers = FXCollections.observableArrayList();
    @FXML ObservableList<String> evaluators = FXCollections.observableArrayList();
    @FXML ObservableList<String> results = FXCollections.observableArrayList();


    public String name;
    public String originalName;

    //Gets called by the FXML Loader when loading the layout file
    public void initialize(){
        System.out.println("initialize");
        listProd.setItems(producers);
        listProd.setCellFactory(param -> new EDListCell());
        //listProd.setPrefHeight(EDListCell.prefHeight);

        listEval.setItems(evaluators);
        listEval.setCellFactory(param -> new EDListCell());
        //listEval.setPrefHeight(EDListCell.prefHeight);

        listResults.setItems(results);
        listResults.setCellFactory(param -> new EDListCell());


        //producers.addListener((ListChangeListener) change -> listProd.setPrefHeight(producers.size() * (EDListCell.prefHeight + 9)));
        //evaluators.addListener((ListChangeListener) change -> listEval.setPrefHeight(evaluators.size() * (EDListCell.prefHeight + 9)));
    }

    public void setName(String n){
        name = n;
        originalName = n;
        titleText.setText(name);
    }

    @FXML public void AddProducer(ActionEvent event){
        producers.add("Producer" + producers.size());
    }
    public void AddProducer(hps.types.Expert expert){
        String n = ((Keyword)(expert).get(Keyword.intern("name"))).getName();
        producers.add(n);
    }

    @FXML public void AddEvaluator(ActionEvent event){
        evaluators.add("Evaluator" + evaluators.size());
    }
    public void AddEvaluator(hps.types.Expert expert){
        String n = ((Keyword)(expert).get(Keyword.intern("name"))).getName();
        evaluators.add(n);
    }

    public HashMap<String,Object> clojureRep ()   {
        this.name = titleText.getText();

        HashMap<String, Object> result = new HashMap<>();
        HashMap<String, HashMap> prods = new HashMap<>();
        HashMap<String, HashMap> evals = new HashMap<>();
        HashMap<String, String> config = new HashMap<>();
        for(String p : producers){
            prods.put(p, new HashMap());
        }
        for(String e : evaluators){
            evals.put(e, new HashMap());
        }

        config.put("evaluator-aggregation-fun", evaluatoraggregationfun.getText());
        result.put("producers", prods);
        result.put("evaluators", evals);
        result.put("config", config);
        result.put("originalname", originalName);
        return result;
    }

    public void setDMConfig(Object o){
        evaluatoraggregationfun.setText(o.toString());
    }

    public void updateResult(hps.types.DecisionResult r){
        if(r != null) {
            String n = BackgroundCanvas.getChain.invoke(r, "decision", "value").toString();
            String v = BackgroundCanvas.getChain.invoke(r, "decision", "aggregated-evaluation").toString();


            Platform.runLater(new Runnable() {
                @Override public void run() {
                    results.clear();
                    results.add(n + ":" + v);
                    clojure.lang.ArraySeq others = (clojure.lang.ArraySeq) r.get(Keyword.intern("retained-alternatives"));
                    for(Object o : others){
                        String on = ((hps.types.DecisionAlternative) o).get(Keyword.intern("value")).toString();
                        String ov = ((hps.types.DecisionAlternative) o).get(Keyword.intern("aggregated-evaluation")).toString();
                        results.add(on + " : " + ov);
                    }
                }
            });
        }
    }
}
