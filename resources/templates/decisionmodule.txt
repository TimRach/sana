

(declare-dm @dmname
            :available-experts {:producers @producers
                                :evaluators @evaluators}
            :configure-fun (fn [_]
                             (map->DecisionSetup
                              {:experts {:producers @producers
                                         :evaluators @evaluators}}))
            :onacceptance-fun @onacceptance-fun
            :decision-parameters {:evaluator-aggregation-fun @evaluator-aggregation-fun
                                  :run-frequency-fun (run-frequency-fun-by-time 100)
                                  :clocked-interval 1})
